
# [Import modules]
from airflow.models import DAG
from lib.sirc.dag_generator import *
from datetime import datetime, timedelta

# [Batch configuration]
BATCH_NAME = "sab-card-equipment-synchronization"
DESCRIPTION = "[SAB-01-05] mise à jour equipement carte dans la Service Layer et si besoin le statut dans les serveurs financiers."
TAGS = ["VDC", "SAB"]
ENVIRONMENTS = ["dev", "test", "acc", "sta", "prod"]
SCHEDULE = "0 01-08 * * 2-6" # Mardi au samedi entre 01h et 08h
# [Default arguments] NOTE: times in Seconds
default_args = {
    "owner": "SIRC",
    "start_date": datetime(2021, 5, 1),
    "email": ["airflow-alert@orangebank.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "depends_on_past": False,
    # auto-restartable ?
    "retries": 0,
    # "retry_delay": RETRY_DELAY,
    "dagrun_timeout": 60*60,
    "sla": 60*60,
    # [Batch configuration]
    "batch_infos": {
        "batch_name": BATCH_NAME,
        "batch_parameters": {},
        # poke-interval: between <15sec> and <15min>
        "slbatch_poll_interval": 15,
        # timeouts: this for polling (+2min for the first call)
        "slbatch_api_call_timeout": 30*60,
        # [START input_files] (optional)
        "use_lc_v1": True,
        "input_files": [
            {
                # general infos
                "partner": "SAB",
                "sirc_flow_id": "SAB-01-05",
                # infos SL-Batch
                "filename_parameter_name": "inputFileName",
                # infos Landing-Component
                "partner_id": "G2S",
                "exchange_protocol": "SFTP",
                "s3_landing_path": "sab-01/sab-01-05/",
                # timedelta is not JSON serializable
                "landing_component_call_timeout": 15*60,
                "filename_pattern": "SABSLB006_*.csv",
                "expected_file_size": "300K",
                ## LC workflow customising
                "exclusive": True
            }
        ]
    }
}

# [DAGs Creation] - don't modify
for env in get_current_dag_environments(ENVIRONMENTS):
    dag = generate_dag(default_args, SCHEDULE, TAGS, DESCRIPTION, env)
    globals()[dag.dag_id] = dag
