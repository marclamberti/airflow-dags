from airflow.providers.http.sensors.http import HttpSensor
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.operators.python import PythonOperator,task
from airflow.utils.task_group import TaskGroup
from airflow.utils.state import State
from airflow.exceptions import AirflowFailException,AirflowSensorTimeout
from datetime import datetime,timedelta
import json
import logging
import os

# [AFA variables] TLS-MA with technical AFT
AIRFLOW_CA_BUNDLE_PATH = 'cloud-airflow-ca-bundle'
AIRFLOW_CLIENT_CERT    = 'cloud-airflow-client-cert'

# [AFT HTTP connection]
LC_API_CONN_ID  = 'cloud-airflow-tech-service-acc-{CLUSTER_ENV}'

# [AFT API]
LC_DAG_ID  = 'landing_component_dag'
LC_TASK_ID = 'transfer_file'

# xCom keys
LC_FILENAME_REMOTE_XCOM_KEY = 'return_value'
LC_FILENAME_LOCAL_XCOM_KEY  = 'returned_filename'
LC_FILENAME_JSON_FIELD = 'value'

# endpoints
LC_API_ENDPOINT_RUN_LC       = 'dags/'+LC_DAG_ID+'/dagRuns'
LC_API_ENDPOINT_CHECK_STATUS = LC_API_ENDPOINT_RUN_LC+'/{DAG_RUN_ID}'
LC_API_ENDPOINT_GET_FILENAME = LC_API_ENDPOINT_RUN_LC+'/{DAG_RUN_ID}/taskInstances/'+LC_TASK_ID+'/xcomEntries/'+LC_FILENAME_REMOTE_XCOM_KEY

# LC actions
LC_ACTION_RECEIVE = 'receive'
LC_ACTION_SEND    = 'send'

# [LC call Tasks]
def _build_landing_component_params(ti,**kwargs):
    logging.info('Start Landing-component API call for batch: {}'.format(kwargs['batch_name']))
    logging.info('LC arguments call: {}'.format(str(kwargs)))
    if 'lc_action' not in kwargs or (kwargs['lc_action'] != LC_ACTION_RECEIVE and kwargs['lc_action'] != LC_ACTION_SEND):
        raise AirflowFailException('Action not supported: ' + lc_action)

    # build paramaters
    lc_action = kwargs['lc_action']
    logging.info('LANDING_COMPONENT_ACTION: ' + lc_action)
    logging.info('PARTNER_ID: ' + kwargs['partner_id'])
    # FIXME: flow in Y
    flowId = kwargs['partner_id'].lower() + '-to-sirc' if LC_ACTION_RECEIVE == lc_action else 'sirc-to-' + kwargs['partner_id'].lower()
    keepFileInSrc = False if LC_ACTION_RECEIVE == lc_action and kwargs['exclusive'] == True else True
    lc_conf = {
        'flow': flowId,
        # FIXME: for the source_filename LC should scan S3/sftp path with prefix/filter 
        # and get files by the-exact-name or name-with-wildcard
        'srcPath': kwargs['source_path'],
        'destPath': kwargs['destination_path'],
        'keepFileInSrc': keepFileInSrc
    }
    ti.xcom_push(key='lc_call_args', value=json.dumps({ 'conf': lc_conf }))
    return None

def verify_run_lc_response(response, **kwargs):
    # FIXME: only 201 is expected here
    if 200 <= response.status_code < 300:
        logging.info(response.json())
        kwargs['ti'].xcom_push('lc_dag_run_id', response.json()['dag_run_id'])
        return True

    if 400 <= response.status_code < 500:
        raise AirflowFailException('Wrong HTTP code received: ' + str(response.status_code))
    # retry
    raise ValueError('Wrong HTTP code received: ' + str(response.status_code))

def verify_check_lc_status_response(response, **kwargs):
    # FIXME: only 200 is expected here
    if 200 <= response.status_code < 300:
        logging.info(response.json())
        lc_dag_run_status = response.json()['state']
        logging.info('LC status: ' + lc_dag_run_status)
        kwargs['ti'].xcom_push('lc_dag_run_status', lc_dag_run_status)
        # check dag-run task status
        if State.SUCCESS == lc_dag_run_status:
            logging.info('Landing component dag_run terminated with Success')
            return True
        if State.QUEUED == lc_dag_run_status or State.RUNNING == lc_dag_run_status or State.UP_FOR_RETRY == lc_dag_run_status or State.UP_FOR_RESCHEDULE == lc_dag_run_status:
            logging.error('Landing component dag_run is still running [{}]'.format(lc_dag_run_status))
            return False
        # FAILED |SHUTDOWN|UPSTREAM_FAILED|SKIPPED
        logging.error('Landing component dag_run failed with status [{}]'.format(lc_dag_run_status))
        raise AirflowFailException('Error on Landing-component call. dag_run state: ' + lc_dag_run_status)
    
    if response.status_code >= 500:
        logging.error('Bad HTTP status: {}. May be a temporary unavailability?'.format(str(response.status_code)))
        kwargs['ti'].xcom_push(key='lc_dag_run_status', value='FALIED_{}'.format(str(response.status_code)))
        return False

    # will cause the task to fail without retry
    logging.error('LC call FAILED with HTTP status: {}'.format(str(response.status_code)))
    kwargs['ti'].xcom_push('lc_dag_run_status', 'FALIED_{}'.format(str(response.status_code)))
    
    # FIXME: 4XX fail without retry.
    if 400 <= response.status_code < 500:
        raise AirflowFailException('Wrong HTTP code received: ' + str(response.status_code))
    # skip polling for retry
    raise ValueError('Wrong HTTP code received: ' + str(response.status_code))

def push_filename_value_in_xCom(response, **kwargs):
    logging.info('retrieve filename from Landing-component call response')
    filename = response.json()[LC_FILENAME_JSON_FIELD]
    logging.info(filename)
    if not len(filename): raise ValueError('returned xCom-value is empty: '  +filename)
    # TODO: verify if return_value is not an exception
    # {'code': 'failed', 'response': 'Error code is 101, message is flow g2s-sirc-incoming not found in the config variable'}
    # {'code': 'failed', 'response': 'Error code is 101, message is Config file /usr/local/airflow/conf/conf.yaml not found'}
    if filename.startswith("{'code'"): raise AirflowFailException('Wrong filename found in xCom. An error occured: ' + filename)
    kwargs['ti'].xcom_push(key=LC_FILENAME_LOCAL_XCOM_KEY, value=filename)
    return True

def call_landing_component_failure_callback(context):
    logging.error(context)
    if isinstance(context['exception'], AirflowSensorTimeout):
        logging.error('*** Task timed out ***')

# [LC call task-group]
def call_landing_component_api(lc_args):

    with TaskGroup(lc_args['lc_task_group_id'], tooltip='Call AFT API (LC): ' + lc_args['lc_action'] + ' [' + lc_args['partner'] + '] file via [' + lc_args['partner_id'] + ']') as call_landing_component_api:

        # build call parameters
        build_landing_component_params = PythonOperator(
            task_id = 'build_landing_component_params',
            python_callable = _build_landing_component_params,
            op_kwargs = lc_args
        )

        # trigger DAG on LC
        call_landing_component = SimpleHttpOperator(
            task_id = 'call_landing_component',
            http_conn_id = LC_API_CONN_ID.format(CLUSTER_ENV=lc_args['batch_cluster_env']),
            method = 'POST',
            endpoint = LC_API_ENDPOINT_RUN_LC,
            data = "{{ task_instance.xcom_pull(key='lc_call_args', task_ids=[task.upstream_list[0].task_id])[0] }}",
            headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            response_filter = lambda response: json.loads(response.text),
            response_check = verify_run_lc_response,
            execution_timeout = timedelta(seconds=120),
            do_xcom_push = False,
            log_response = True,
            on_failure_callback = call_landing_component_failure_callback,
            extra_options = {
                'check_response': False,
                'verify': False
                # TODO: [TLS-MA with Tech-Airflow API]
                #'verify': Variable.get(AIRFLOW_CA_BUNDLE_PATH),
                #'cert': Variable.get(AIRFLOW_CLIENT_CERT)
            }
        )

        # wait for response
        poll_landing_component = HttpSensor(
            task_id = 'poll_landing_component',
            http_conn_id = LC_API_CONN_ID.format(CLUSTER_ENV=lc_args['batch_cluster_env']),
            method = 'GET',
            endpoint = LC_API_ENDPOINT_CHECK_STATUS.format(DAG_RUN_ID="{{ ti.xcom_pull(key='lc_dag_run_id',task_ids=[task.upstream_list[0].task_id])[0] }}"),
            headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            response_check = verify_check_lc_status_response,
            poke_interval = 15,
            exponential_backoff = True,
            mode = 'reschedule',
            # FIXME: timeout vs execution_timeout
            timeout = lc_args['landing_component_call_timeout'],
            execution_timeout = timedelta(seconds=lc_args['landing_component_call_timeout']),
            retries = 5,
            retry_delay = timedelta(seconds=5*60),
            retry_exponential_backoff = True,
            max_retry_delay = timedelta(seconds=15*60),
            on_failure_callback = call_landing_component_failure_callback,
            extra_options = {
                'check_response': False,
                'verify': False
                # TODO: [TLS-MA with Tech-Airflow API]
                #'verify': Variable.get(AIRFLOW_CA_BUNDLE_PATH),
                #'cert': Variable.get(AIRFLOW_CLIENT_CERT)
            }
        )

        # retrieve file-name and push it into xCom
        retrieve_returned_filename = SimpleHttpOperator(
            task_id = 'retrieve_returned_filename',
            http_conn_id = LC_API_CONN_ID.format(CLUSTER_ENV=lc_args['batch_cluster_env']),
            method = 'GET',
            endpoint = LC_API_ENDPOINT_GET_FILENAME.format(DAG_RUN_ID="{{ ti.xcom_pull(key='lc_dag_run_id',task_ids=[task.upstream_list[0].upstream_list[0].task_id])[0] }}"),
            response_filter = lambda response: json.loads(response.text),
            response_check = push_filename_value_in_xCom,
            execution_timeout = timedelta(seconds=120),
            do_xcom_push = False,
            log_response = True,
            on_failure_callback = call_landing_component_failure_callback,
            extra_options = {
                'check_response': False,
                'verify': False
                # TODO: [TLS-MA with Tech-Airflow API]
                #'verify': Variable.get(AIRFLOW_CA_BUNDLE_PATH),
                #'cert': Variable.get(AIRFLOW_CLIENT_CERT)
            }
        )

        build_landing_component_params >> call_landing_component >> poll_landing_component >> retrieve_returned_filename

    return call_landing_component_api
