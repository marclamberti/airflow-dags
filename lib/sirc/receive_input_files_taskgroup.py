from airflow.operators.python import PythonOperator
from airflow.utils.task_group import TaskGroup
from airflow.sensors.s3_key_sensor import S3KeySensor
from airflow.hooks.S3_hook import S3Hook

from lib.sirc.call_landing_component_taskgroup import *
from datetime import datetime,timedelta
from copy import deepcopy
import logging
import os

# [Constants]
S3_LANDING_PATH_IN = 'batch/{ENV}in-process/{SUB_PATH}'
S3_STAGING_PATH_IN = 'batch/{ENV}{BATCH_NAME}/to-process/'

TASK_LC_GENERATE_FILENAME = 'receive_input_files.receive_file{RANK}_from_partner.retrieve_returned_filename'

# [Functions]
def isHasInputFile(kwargs):
    return "input_files" in kwargs and len(kwargs['input_files'])

def _start_receiving_files(ti, **kwargs):
    if not isHasInputFile(kwargs):
        raise AirflowFailException('Landing-Component call failed. <input_files> entry is missed in batch args: {}'.format(str(kwargs)))

    logging.info('Start Landing-Component call (InputFile) for batch: {}'.format(kwargs['batch_name']))
    return None

def _end_receiving_files(ti, **kwargs):
    input_filenames = []
    for idx,input_file in enumerate(kwargs['input_files']):
        input_filenames.append(ti.xcom_pull(key=LC_FILENAME_LOCAL_XCOM_KEY, task_ids=[TASK_LC_GENERATE_FILENAME.format(RANK=str(idx+1))])[0])
    logging.info(input_filenames)
        
    # FIXME: assert
    if not len(input_filenames) or input_filenames[0] is None or len(kwargs['input_files']) != len(input_filenames):
        raise AirflowFailException('xCom <input_filenames>:{} does not match batchs arguments <input_files>:{}'.format(str(len(input_filenames)),str(len(kwargs['input_files']))))

    ti.xcom_push(key='input_filenames', value=input_filenames)
    # TODO: verify filenames patterns <filename_pattern>
    logging.info('End Landing-Component call (InputFile) for batch: {}'.format(kwargs['batch_name']))
    return None

def build_lc_args(dag_args, idx):
    # copy from partner (sftp-server|s3-landing-bucket) to slbatch s3-staging bucket
    lc_args = deepcopy(dag_args['batch_infos']['input_files'][idx])
    lc_args['index'] = idx
    lc_args['batch_env'] = dag_args['batch_infos']['batch_env']
    lc_args['batch_cluster_env'] = dag_args['batch_infos']['batch_cluster_env']
    lc_args['batch_name'] = dag_args['batch_infos']['batch_name']
    lc_args['lc_task_group_id'] = 'receive_file{RANK}_from_partner'.format(RANK=str(idx+1))
    lc_args['lc_action'] = LC_ACTION_RECEIVE
    
    # assert source path configuration
    if 's3_landing_path' not in lc_args and 'sftp_partner_path' not in lc_args:
        # FIXME: AirflowConfigException, AirflowFailException, AirflowFileParseException
        raise AirflowConfigException('Erreur de configration DAG: <input_files> doit avoir un attribut <s3_landing_path> ou <sftp_partner_path>')

    env_prefix  = '' if lc_args['batch_env'] == 'prod' else lc_args['batch_env'] + '/'

    # source file: SFTP or Partner S3 Landing
    if 's3_landing_path' in lc_args and len(lc_args['s3_landing_path']):
        lc_args['source_path'] = S3_LANDING_PATH_IN.format(ENV=env_prefix, SUB_PATH=lc_args['s3_landing_path'])
    elif 'sftp_partner_path' in lc_args and len(lc_args['sftp_partner_path']):
        lc_args['source_path'] = lc_args['sftp_partner_path']
    # FIXME: unknown file name at source (this should contains wildcard *)
    lc_args['source_path'] = lc_args['source_path'] + lc_args['filename_pattern']

    # destination file: S3 Staging
    # NOTE: LC generate final filename
    lc_args['destination_path'] = S3_STAGING_PATH_IN.format(ENV=env_prefix, BATCH_NAME=dag_args['batch_infos']['batch_name'])

    return lc_args

# [Tasks]
def receive_input_files(dag_args):

    batch_infos = dag_args['batch_infos']

    with TaskGroup("receive_input_files", tooltip="Recieve files from Partner - call LC Upstream)") as receive_input_files:

        # start LC call
        start_receiving_files = PythonOperator(
            task_id = 'start_receiving_files',
            python_callable = _start_receiving_files,
            op_kwargs = batch_infos
        )

        # trigger api call and poll response
        input_file_tasks = []
        for idx,output_file in enumerate(batch_infos['input_files']):
            input_file_tasks.append(call_landing_component_api(build_lc_args(dag_args, idx)))

        # end LC call
        end_receiving_files = PythonOperator(
            task_id = 'end_receiving_files',
            python_callable = _end_receiving_files,
            trigger_rule = 'all_success', # default
            op_kwargs = batch_infos
        )

        start_receiving_files >> input_file_tasks >> end_receiving_files

    return receive_input_files
