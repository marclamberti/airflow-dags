from airflow.providers.http.sensors.http import HttpSensor
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.operators.python import PythonOperator
from airflow.utils.task_group import TaskGroup
from airflow.exceptions import AirflowFailException,AirflowSensorTimeout
from datetime import datetime,timedelta
import json
import logging

# [Constants]
SLBATCH_API_ENDPOINT_RUN_BATCH    = 'services/api/batch/{BATCH_NAME}/async'
SLBATCH_API_ENDPOINT_CHECK_STATUS = 'services/api/batch/{BATCH_NAME}/{JOB_EXECUTION_ID}'

# [Airflow connections]
SLBATCH_API_CONN_ID    = 'sirc-slbatch-api{ENV}' # Base url for SL-Batch

# [Airflow variables] TLS-MA with SL-Batch API
SLBATCH_CA_BUNDLE_PATH = 'sirc-slbatch-api-ca-bundle'
AIRFLOW_CLIENT_CERT    = 'cloud-airflow-client-cert'

# [Functions]
# Function to build arguments for SL-Batch API call 
def _build_slbatch_api_params(**kwargs):
    logging.info('Start SL-Batch API call for batch: {}'.format(kwargs['batch_name']))
    params = {}
    # 1.add static parameters -in DAG configuration
    add_dag_static_parameters(params, kwargs)
    
    # 2.add input filename(s) parameter(s) -if LC is called upstream
    add_received_filenames_parameters(params, kwargs)
    
    # 3.add output filename(s) parameter(s) -if LC is called downstream
    add_sent_filenames_parameters(params, kwargs)

    # 4.add config parameter names -if triggered via airflow GUI or CLI
    add_trigger_config_parameters(params, kwargs)

    logging.info('slbatch_call_args: {}'.format(str(params)))
    kwargs['ti'].xcom_push(key='slbatch_call_args', value=json.dumps(params))
    return None

def add_dag_static_parameters(params, kwargs):
    if 'batch_parameters' not in kwargs or not len(kwargs['batch_parameters']):
        return params
    for key in kwargs['batch_parameters']:
        params[str(key)] = str(kwargs['batch_parameters'][key])
    return params

def add_received_filenames_parameters(params, kwargs):
    # NOTE: check if landing component is called
    if 'receive_input_files.end_receiving_files' not in kwargs['task'].upstream_task_ids:
        return params
    if 'input_files' not in kwargs or not len(kwargs['input_files']):
        return params

    # get filenames
    input_filenames = kwargs['ti'].xcom_pull(key='input_filenames', task_ids=['receive_input_files.end_receiving_files'])
    logging.info(input_filenames)
    # FIXME: this assert should be already done by landing-component-call task
    if not len(input_filenames) or input_filenames[0] is None or len(list(kwargs['input_files'])) != len(input_filenames[0]):
        raise AirflowFailException('xCom <input_filenames> count:{} does not match batch-args count:{}'.format(str(len(input_filenames[0])),str(len(list(kwargs['input_files'])))))

    for idx,input_filename in enumerate(input_filenames[0]):
        logging.info('input_filename[{}]: {}'.format(str(idx),input_filename))
        # NOTE: the filenames parameters are only added if batch_args contains attr <filename_parameter_name>
        # if the batch dosen't expect the filenames in the request, remove <filename_parameter_name> attr.
        if 'filename_parameter_name' in kwargs['input_files'][idx] and len(kwargs['input_files'][idx]['filename_parameter_name']):
            params[str(kwargs['input_files'][idx]['filename_parameter_name'])] = input_filename

    return params

def add_sent_filenames_parameters(params, kwargs):
    if 'output_files' not in kwargs or not len(kwargs['output_files']):
        return params

    for idx,output_filename in enumerate(kwargs['output_files']):
        logging.info('output_filename[{}]: {}'.format(str(idx),output_filename))
        # NOTE: the filenames parameters are only added if batch_args contains attr <filename_parameter_name>
        # if the batch dosen't expect the filenames in the request, remove <filename_parameter_name> attr.
        if 'filename_parameter_name' in kwargs['output_files'][idx] and len(kwargs['output_files'][idx]['filename_parameter_name']):
            params[str(kwargs['output_files'][idx]['filename_parameter_name'])] = output_filename['filename_pattern']

    return params

def add_trigger_config_parameters(params, kwargs):
    if not len(kwargs['dag_run'].conf): 
        return params
    for key in kwargs['dag_run'].conf:
        params[str(key)] = str(kwargs['dag_run'].conf[key])
    return params

# Function to check the run batch HTTP response from the SL Batch
def verify_run_batch_api_response(response, **kwargs):
    if response.status_code == 202:
        job_execution_id = str(response.json()['id'])
        logging.info('job_execution_id: {}'.format(job_execution_id))
        kwargs['ti'].xcom_push(key='job_execution_id', value=job_execution_id)
        return True

    if 400 <= response.status_code < 500:
        raise AirflowFailException('Wrong HTTP code received: ' + str(response.status_code))
    # retry
    raise ValueError('Wrong HTTP code received: ' + str(response.status_code))

# Function to handle the status of the batch and push/update it in xCom
def verify_check_batch_status_api_response(response, **kwargs):
    logging.info(response.json())
    # stop polling
    if response.status_code == 200:
        logging.info('Batch terminated with Success. status: {}'.format(str(response.status_code)))
        kwargs['ti'].xcom_push(key='batch_status', value='COMPLETED')
        return True
    if response.status_code == 206:
        logging.error('Batch terminated with rejected-lines. status: {}'.format(str(response.status_code)))
        kwargs['ti'].xcom_push(key='batch_status', value='COMPLETED_WITH_REJECTION')
        return True
    if response.status_code == 406:
        logging.error('Batch failed with HTTP status: {}'.format(str(response.status_code)))
        kwargs['ti'].xcom_push(key='batch_status', value='FAILED')
        return True

    # continue polling
    if response.status_code == 102:
        logging.info('Batch still running. status: {}'.format(str(response.status_code)))
        kwargs['ti'].xcom_push(key='batch_status', value='RUNNING')
        return False
    if response.status_code >= 500:
        logging.error('Bad HTTP status: {}. May be a temporary unavailability?'.format(str(response.status_code)))
        kwargs['ti'].xcom_push(key='batch_status', value='FALIED_{}'.format(str(response.status_code)))
        return False

    logging.error('Batch FAILED with HTTP status: {}'.format(str(response.status_code)))
    kwargs['ti'].xcom_push(key='batch_status', value='FALIED_{}'.format(str(response.status_code)))

    # FIXME: 4XX fail without retry.
    if 400 <= response.status_code < 500:
        raise AirflowFailException('Wrong HTTP code received: ' + str(response.status_code))
    # skip polling for retry
    raise ValueError('Wrong HTTP code received: ' + str(response.status_code))

# Function to check the result of end-to-end SL-Batch API call
def _process_slbatch_api_response(**kwargs):
    logging.info('End SL-Batch API call for batch: {}'.format(kwargs['batch_name']))
    batch_status = kwargs['ti'].xcom_pull(key='batch_status', task_ids=['run_batch.poll_slbatch_api'])[0]
    logging.info('Batch status: {}'.format(batch_status))
    # check status
    if batch_status == 'COMPLETED':
        logging.info('Batch terminated with Success')
        return None
    elif batch_status == 'COMPLETED_WITH_REJECTION':
        logging.error('Batch terminated with rejected-lines [{}]'.format(batch_status))
        raise ValueError('Batch terminated with rejected-lines. mark this task as success after file analysis')
    elif batch_status == 'RUNNING':
        logging.error('Batch is still running may be a timeout problem [{}]'.format(batch_status))
        raise ValueError('Batch still running. retry from polling task')

    # UNKNWON STATUS: without retry here
    logging.error('Batch failed with unknwon status [{}]'.format(batch_status))
    raise AirflowFailException('Batch in error: {}'.format(batch_status))

def call_slbatch_failure_callback(context):
    logging.error(context)
    if isinstance(context['exception'], AirflowSensorTimeout):
        logging.error('*** Task timed out ***')

# Perform an API call to SL-Batch API vi HTTP
def run_batch(dag_args):

    batch_infos = dag_args['batch_infos']
    env_suffix  = '' if batch_infos['batch_env'] == 'prod' else '-' + batch_infos['batch_env']

    with TaskGroup('run_batch', tooltip='Call SL-Batch API: Launch batch [' + batch_infos['batch_name'] + ']') as run_batch_taskgroup:

        # prepare call parameters
        build_slbatch_api_params = PythonOperator(
            task_id = 'build_slbatch_api_params',
            python_callable = _build_slbatch_api_params,
            op_kwargs = batch_infos
        )

        # Perform an HTTP call (with the file in parameter to process it)
        call_slbatch_api = SimpleHttpOperator(
            task_id = 'call_slbatch_api',
            http_conn_id = SLBATCH_API_CONN_ID.format(ENV=env_suffix),
            method = 'POST',
            endpoint = SLBATCH_API_ENDPOINT_RUN_BATCH.format(BATCH_NAME=batch_infos['batch_name']),
            data = "{{ task_instance.xcom_pull(key='slbatch_call_args', task_ids=['run_batch.build_slbatch_api_params'])[0] }}",
            headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            do_xcom_push = False,
            response_check = verify_run_batch_api_response,
            response_filter = lambda response: json.loads(response.text),
            log_response = True,
            execution_timeout = timedelta(seconds=120),
            on_failure_callback = call_slbatch_failure_callback,
            extra_options = {
                'check_response': False,
                'verify': False
                # TODO: [TLS-MA with SL-Batch API]
                #'verify': Variable.get(SLBATCH_CA_BUNDLE_PATH),
                #'cert': Variable.get(AIRFLOW_CLIENT_CERT)
            }
        )

        # Look for the end of the batch execution by polling periodically
        poll_slbatch_api = HttpSensor(
            task_id = 'poll_slbatch_api',
            http_conn_id = SLBATCH_API_CONN_ID.format(ENV=env_suffix),
            method = 'GET',
            endpoint = SLBATCH_API_ENDPOINT_CHECK_STATUS.format(BATCH_NAME=batch_infos['batch_name'], JOB_EXECUTION_ID="{{ task_instance.xcom_pull(key='job_execution_id', task_ids=['run_batch.call_slbatch_api'])[0] }}"),
            headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            response_check = verify_check_batch_status_api_response,
            mode = 'reschedule',
            poke_interval = batch_infos['slbatch_poll_interval'],
            exponential_backoff = True,
            # FIXME: timeout vs execution_timeout
            timeout = batch_infos['slbatch_api_call_timeout'],
            execution_timeout = timedelta(seconds=batch_infos['slbatch_api_call_timeout']),
            retries = 5,
            retry_delay = timedelta(seconds=5*60),
            retry_exponential_backoff = True,
            max_retry_delay = timedelta(seconds=15*60),
            # soft_fail = True, # mark the task as SKIPPED on failure ?
            on_failure_callback = call_slbatch_failure_callback,
            extra_options = {
                'check_response': False,
                'verify': False
                # TODO: [TLS-MA with SL-Batch API]
                #'verify': Variable.get(SLBATCH_CA_BUNDLE_PATH),
                #'cert': Variable.get(AIRFLOW_CLIENT_CERT)
            }
        )

        # check the result after sensor
        process_slbatch_api_response = PythonOperator(
            task_id = 'process_slbatch_api_response',
            python_callable = _process_slbatch_api_response,
            retries = 0,
            op_kwargs = batch_infos
        )

        build_slbatch_api_params >> call_slbatch_api >> poll_slbatch_api >> process_slbatch_api_response

    return run_batch_taskgroup