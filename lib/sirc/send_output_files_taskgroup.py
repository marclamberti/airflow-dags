from airflow.operators.python import PythonOperator
from airflow.utils.task_group import TaskGroup
from lib.sirc.call_landing_component_taskgroup import *
from datetime import datetime,timedelta
from copy import deepcopy
import logging

# [Constants]
S3_STAGING_PATH_OUT = 'batch/{ENV}{BATCH_NAME}/{JOB_EXECUTION_ID}/out-process/'
S3_LANDING_PATH_OUT = '{ENV}out-process/{SUB_PATH}'

TASK_LC_GENERATE_FILENAME = 'send_output_files.send_file{RANK}_to_partner.retrieve_returned_filename'

# [Functions]
def isHasOutputFile(kwargs):
    return "output_files" in kwargs and len(kwargs['output_files'])

def _start_sending_files(ti, **kwargs):
    if not isHasOutputFile(kwargs):
        raise AirflowFailException('Landing-Component call failed. <output_files> entry is missed in batch args: {}'.format(str(kwargs)))

    # extract job_execution_id from xCom
    logging.info('Start Landing-Component call (OutputFile) for batch: {}'.format(kwargs['batch_name']))
    job_execution_id = ti.xcom_pull(key='job_execution_id', task_ids=['run_batch.call_slbatch_api'])
    if not len(job_execution_id) or job_execution_id[0] is None:
        raise AirflowFailException('Landing-Component call failed. <job_execution_id> is missed in xCom')

    return None

def _end_sending_files(ti, **kwargs):
    output_filenames = []
    for idx,output_file in enumerate(kwargs['output_files']):
        output_filenames.append(ti.xcom_pull(key=LC_FILENAME_LOCAL_XCOM_KEY, task_ids=[TASK_LC_GENERATE_FILENAME.format(RANK=str(idx+1))])[0])
    logging.info(output_filenames)

    # FIXME: assert
    if not len(output_filenames) or output_filenames[0] is None or len(kwargs['output_files']) != len(output_filenames):
        raise AirflowFailException('xCom <output_filenames>:{} does not match batchs arguments <output_files>:{}'.format(str(len(output_filenames)),str(len(kwargs['output_files']))))

    ti.xcom_push(key= 'output_filenames', value=output_filenames)
    # TODO: verify filenames patterns <filename_pattern>
    logging.info('End Landing-Component call (OutputFile) for batch: {}'.format(kwargs['batch_name']))
    return None

def build_lc_args(dag_args, idx):
    # copy from slbatch s3-staging bucket to partner (sftp-server|s3-landing-bucket)
    lc_args = deepcopy(dag_args['batch_infos']['output_files'][idx])
    lc_args['index'] = idx
    lc_args['batch_env'] = dag_args['batch_infos']['batch_env']
    lc_args['batch_cluster_env'] = dag_args['batch_infos']['batch_cluster_env']
    lc_args['batch_name'] = dag_args['batch_infos']['batch_name']
    lc_args['lc_task_group_id'] = 'send_file{RANK}_to_partner'.format(RANK=str(idx+1))
    lc_args['lc_action'] = LC_ACTION_SEND

    # assert destination path configuration
    if 's3_landing_path' not in lc_args and 'sftp_partner_path' not in lc_args:
        # FIXME: AirflowConfigException, AirflowFailException, AirflowFileParseException
        raise AirflowConfigException('Erreur de configration DAG: <input_files> doit avoir un attribut <s3_landing_path> ou <sftp_partner_path>')

    env_prefix  = '' if lc_args['batch_env'] == 'prod' else lc_args['batch_env'] + '/'

    # source file: extract jobExecutionId
    job_execution_id = "{{ ti.xcom_pull(key='job_execution_id', task_ids=['run_batch.call_slbatch_api'])[0] }}"
    lc_args['source_path'] = S3_STAGING_PATH_OUT.format(ENV=env_prefix, BATCH_NAME=dag_args['batch_infos']['batch_name'],JOB_EXECUTION_ID=job_execution_id) + lc_args['filename_pattern']

    # destination file: SFTP or Partner S3 Landing
    # NOTE: LC generate final filename
    if 's3_landing_path' in lc_args and len(lc_args['s3_landing_path']):
        lc_args['destination_path'] = S3_LANDING_PATH_OUT.format(ENV=env_prefix, SUB_PATH=lc_args['s3_landing_path'])
    elif 'sftp_partner_path' in lc_args and len(lc_args['sftp_partner_path']):
        lc_args['destination_path'] = lc_args['sftp_partner_path']

    return lc_args

# [Tasks]
def send_output_files(dag_args):

    batch_infos = dag_args['batch_infos']

    with TaskGroup("send_output_files", tooltip="Send files to Partner - call LC Downstream") as send_output_files:
        
        # start LC call
        start_sending_files = PythonOperator(
            task_id = 'start_sending_files',
            python_callable = _start_sending_files,
            op_kwargs = batch_infos
        )

        # trigger api call and poll response
        output_file_tasks = []
        for idx,output_file in enumerate(batch_infos['output_files']):
            output_file_tasks.append(call_landing_component_api(build_lc_args(dag_args, idx)))

        # end LC call
        end_sending_files = PythonOperator(
            task_id = 'end_sending_files',
            python_callable = _end_sending_files,
            trigger_rule = 'all_success', # default
            op_kwargs = batch_infos
        )
        
        start_sending_files >> output_file_tasks >> end_sending_files

    return send_output_files
