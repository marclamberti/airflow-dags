from airflow.models import DAG
from airflow.models import Variable
from airflow.operators.python import task

from lib.sirc.call_sl_batch_api_taskgroup import run_batch
from lib.sirc.receive_input_files_taskgroup import *
from lib.sirc.receive_input_files_lcv1_taskgroup import *
from lib.sirc.send_output_files_taskgroup import *
from lib.sirc.send_output_files_lcv1_taskgroup import *

from datetime import datetime
from copy import deepcopy
import logging

# [Constants]
AIRFLOW_LIST_ENV_VARIABLE = 'sirc-slbatch-api-envs'
DAG_ID_PATTERN = 'sirc-{BATCH_NAME}{SUFFIX}-dag{ENV}'

# [Tasks]
@task
def start_dag(dag_id):
    logging.info('============ START DAG <{}>'.format(dag_id))
    return None

@task
def end_dag(dag_id):
    logging.info('============ END DAG <{}>'.format(dag_id))
    return None

# TODO: should send alert
def process_sla_miss(dag, task_list, blocking_task_list, slas, blocking_tis):
    print('SLA was missed on DAG %(dag)s by task id %(blocking_tis)s with task list %(task_list)s which are blocking %(blocking_task_list)s' % locals())

# [DAG generatiohn]
def generate_dag_internal(dag_id, dag_schedule, dag_tags, dag_arguments):

    dag = DAG(dag_id, schedule_interval=dag_schedule, default_args=dag_arguments, 
        tags=dag_tags, description=dag_arguments['dag_description'], dagrun_timeout=dag_arguments['dagrun_timeout'], 
        sla_miss_callback=process_sla_miss, catchup=False)

    requireCallToLcUpstream = isHasInputFile(dag_arguments['batch_infos']) and 'dev' != dag_arguments['batch_infos']['batch_env']
    requireCallToLcDownstream = isHasOutputFile(dag_arguments['batch_infos']) and 'dev' != dag_arguments['batch_infos']['batch_env']

    # TODO: to be removed after migration on LCv2
    useLcV1 = dag_arguments['batch_infos']['use_lc_v1']

    with dag:
        
        # [Tasks]
        _start_dag = start_dag(dag_id)

        # [optional] receive input-files from partner via landing-component into sirc-s3bucket-staging
        if requireCallToLcUpstream:
            _receive_input_files = receive_input_files_lcv1(dag_arguments) if useLcV1 else receive_input_files(dag_arguments)

        # call sl-batch api and wait for result result (polling)
        _call_batch_api = run_batch(dag_arguments)

        # [optional] send output-files from sirc-s3bucket-staging to partner via landing-component        
        if requireCallToLcDownstream:            
            _send_output_files = send_output_files_lcv1(dag_arguments) if useLcV1 else send_output_files(dag_arguments)

        _end_dag = end_dag(dag_id)

        # the 4 workflows
        if requireCallToLcUpstream:
            _start_dag >> _receive_input_files >> _call_batch_api
        else:
            _start_dag >> _call_batch_api

        if requireCallToLcDownstream:
            _call_batch_api >> _send_output_files >> _end_dag
        else:
            _call_batch_api >> _end_dag

    return dag

# generate dag for every evironment
def generate_dag(default_args, dag_schedule, default_tags, description, env):
    env_suffix  = '' if env == 'prod' else '-' + env
    # DAG ID   
    # FIXME: suffix for specific DAG's name. RIO: (-ORANGE|-GROUPAMA) (-SMS|-PUSH)
    dag_suffix = '' if 'batch_name_suffix' not in default_args['batch_infos'] else default_args['batch_infos']['batch_name_suffix']
    dag_id = DAG_ID_PATTERN.format(BATCH_NAME=default_args['batch_infos']['batch_name'], SUFFIX=dag_suffix,ENV=env_suffix)

    # DAG arguments
    dag_args = deepcopy(default_args)
    dag_args['dag_description'] = description + ' [' + env + ']'
    dag_args['batch_infos']['batch_env'] = env
    dag_args['batch_infos']['batch_cluster_env'] = 'prod' if env == 'prod' else 'dta'
    dag_args['batch_infos']['use_lc_v1'] = True if 'use_lc_v1' in dag_args['batch_infos'] and dag_args['batch_infos']['use_lc_v1'] == True else False

    # DAG tags
    dag_tags = deepcopy(default_tags)
    dag_tags.append(env)
    if isHasInputFile(dag_args['batch_infos']) and 'dev' != env:
        dag_tags.append('InputFile')
    if isHasOutputFile(dag_args['batch_infos']) and 'dev' != env:
        dag_tags.append('OutputFile')

    if dag_args['batch_infos']['use_lc_v1'] == True:
        dag_tags.append('LCv1')

    return generate_dag_internal(dag_id, dag_schedule, dag_tags, dag_args)

# calculate current possibles environments for dag
def get_current_dag_environments(batch_envs):
    return set(Variable.get(AIRFLOW_LIST_ENV_VARIABLE).split(',')).intersection(batch_envs)
