from airflow.operators.python import PythonOperator
from airflow.utils.task_group import TaskGroup
from airflow.hooks.S3_hook import S3Hook

import logging
import os

# [Constants]
# AIRFLOW_S3_STAGING_CONN_ID = 'sirc_s3_staging_conn_{CLUSTER_ENV}'
S3_STAGING_BUCKET_NAME = 'bank-fr-sftp-staging-sirc-{CLUSTER_ENV}-eu-west-1' # TODO: supprimer "-sftp"
S3_STAGING_PATH_OUT = 'batch/{ENV}{BATCH_NAME}{JOB_EXECUTION_ID}/out-process/'

# [Functions]
# TODO: to be removed after migration on LCv2
# FIXME: Temporary code to keep compatibility with LC v1 (only 1st file is checked!)
def _retrieve_generated_file(ti, **kwargs):
    batch_infos = kwargs['batch_infos']
    env_prefix  = '' if batch_infos['batch_env'] == 'prod' else batch_infos['batch_env'] + '/'
    # source file: extract jobExecutionId
    job_execution_id = ti.xcom_pull(key='job_execution_id', task_ids=['run_batch.call_slbatch_api'])[0]
    source_path = S3_STAGING_PATH_OUT.format(ENV=env_prefix, BATCH_NAME=batch_infos['batch_name'],JOB_EXECUTION_ID='/'+job_execution_id) + batch_infos['output_files'][0]['filename_pattern']
    # s3 = S3Hook(aws_conn_id=AIRFLOW_S3_STAGING_CONN_ID.format(CLUSTER_ENV=batch_infos['cluster_env']))
    s3 = S3Hook(aws_conn_id=None)
    s3obj = s3.get_wildcard_key(wildcard_key=source_path, bucket_name=S3_STAGING_BUCKET_NAME.format(CLUSTER_ENV=batch_infos['batch_cluster_env']), delimiter='/')
    if not s3obj or not len(s3obj): raise ValueError('File not found or not generated: ' + source_path)
    ti.xcom_push(key=LC_FILENAME_LOCAL_XCOM_KEY, value=os.path.basename(s3obj.key))
    return None

def _copy_generated_file(ti, **kwargs):
    output_filename = ti.xcom_pull(key=LC_FILENAME_LOCAL_XCOM_KEY, task_ids=[kwargs['task'].upstream_list[0].task_id])[0]
    logging.info(output_filename)    
    # copy s3 object
    source_path = S3_STAGING_PATH_OUT.format(ENV=env_prefix, BATCH_NAME=batch_infos['batch_name'],JOB_EXECUTION_ID='/'+job_execution_id) + output_filename
    destination_path = S3_STAGING_PATH_OUT.format(ENV=env_prefix, BATCH_NAME=batch_infos['batch_name'],JOB_EXECUTION_ID='') + output_filename
    s3.copy_object(source_bucket_key=source_path, dest_bucket_key=destination_path, 
        source_bucket_name=S3_STAGING_BUCKET_NAME.format(CLUSTER_ENV=batch_infos['batch_cluster_env']),
        dest_bucket_name=S3_STAGING_BUCKET_NAME.format(CLUSTER_ENV=batch_infos['batch_cluster_env']))
    ti.xcom_push(key=LC_FILENAME_LOCAL_XCOM_KEY, value=output_filename)
    return None

# [Tasks]
def send_output_files_lcv1(dag_args):
    batch_infos = dag_args['batch_infos']

    with TaskGroup("send_output_files") as send_output_files:
        retrieve_generated_file = PythonOperator(
            task_id = 'retrieve_generated_file',
            python_callable = _retrieve_generated_file,
            op_kwargs = dag_args
        )
        end_sending_files = PythonOperator(
            task_id = 'end_sending_files',
            python_callable = _copy_generated_file,
            op_kwargs = dag_args
        )        
        retrieve_generated_file >> end_sending_files

    return send_output_files
