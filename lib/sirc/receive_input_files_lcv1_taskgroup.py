from airflow.operators.python import PythonOperator
from airflow.utils.task_group import TaskGroup
from airflow.sensors.s3_key_sensor import S3KeySensor
from airflow.hooks.S3_hook import S3Hook

import logging
import os

# [Constants]
# AIRFLOW_S3_STAGING_CONN_ID = 'sirc_s3_staging_conn_{CLUSTER_ENV}'
S3_STAGING_PATH_IN     = 'batch/{ENV}{BATCH_NAME}/to-process/'
S3_STAGING_BUCKET_NAME = 'bank-fr-sftp-staging-sirc-{CLUSTER_ENV}-eu-west-1' # TODO: supprimer "-sftp"

# [Functions]
# TODO: to be removed after migration on LCv2
# FIXME: Temporary code to keep compatibility with LC v1 (only 1st file is checked!)
def _retrieve_filename(ti, **kwargs):
    batch_infos = kwargs['batch_infos']
    # s3 = S3Hook(aws_conn_id=AIRFLOW_S3_STAGING_CONN_ID.format(CLUSTER_ENV=batch_infos['cluster_env']))
    s3 = S3Hook(aws_conn_id=None)
    s3obj = s3.get_wildcard_key(wildcard_key=batch_infos['destination_path'], bucket_name=S3_STAGING_BUCKET_NAME.format(CLUSTER_ENV=batch_infos['batch_cluster_env']), delimiter='/')
    if not s3obj or not len(s3obj): raise ValueError('File not found or not received: ' + batch_infos['destination_path'])
    ti.xcom_push(key=LC_FILENAME_LOCAL_XCOM_KEY, value=os.path.basename(s3obj.key))
    return None

# [Tasks]
def receive_input_files_lcv1(dag_args):
    batch_infos = dag_args['batch_infos']
    env_prefix  = '' if batch_infos['batch_env'] == 'prod' else batch_infos['batch_env'] + '/'
    batch_infos['destination_path'] = S3_STAGING_PATH_IN.format(ENV=env_prefix, BATCH_NAME=batch_infos['batch_name']) + batch_infos['input_files'][0]['filename_pattern']

    with TaskGroup('receive_input_files') as receive_input_files:
        check_file_presence = S3KeySensor(
            task_id = 'check_file_presence',
            bucket_key = batch_infos['destination_path'],
            wildcard_match = True,
            bucket_name = S3_STAGING_BUCKET_NAME.format(CLUSTER_ENV=batch_infos['batch_cluster_env']),
            # aws_conn_id = AIRFLOW_S3_STAGING_CONN_ID.format(CLUSTER_ENV=batch_infos['batch_cluster_env']),
            aws_conn_id = None,
            verify = False, # FIXME: path/to/cert/bundle.pem
            mode = 'reschedule',
            poke_interval = 5*60,
            timeout = 30*60
        )
        end_receiving_files = PythonOperator(
            task_id = 'end_receiving_files',
            python_callable = _retrieve_filename,
            op_kwargs = dag_args
        )
        check_file_presence >> end_receiving_files

    return receive_input_files
