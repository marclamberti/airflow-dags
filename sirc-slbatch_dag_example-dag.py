# [Import modules]
from airflow.models import DAG
from lib.sirc.dag_generator import *
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

# [Batch configuration]
BATCH_NAME = "slbatch_dag_example"
DESCRIPTION = "[SAB-01-02] Exemple pour la configuration des DAGs sirc - Appel SlBatch Api (+AFT Api)"
SCHEDULE = None
TAGS = ["FLEX", "SAB", "Franfinance", "iSoft"]
ENVIRONMENTS = ["dev", "test", "prod"]

# [Default arguments] NOTE: times in <Seconds>
default_args = {
    "owner": "SIRC",
    "start_date": datetime(2021, 4, 10),
    "email": ["airflow-alert@orangebank.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "depends_on_past": False,
    "retries": 3,
    "retry_delay": timedelta(seconds=5*60),
    # timeout: the whole process
    "dagrun_timeout": 30*60,
    "sla": 30*60,
    # [Batch configuration]
    "batch_infos": {
        "batch_name": BATCH_NAME,
        "batch_parameters": {
            "network_distributor_param": "ORANGE-FRANCE",  # static param
            "cardExpirationDate": str((datetime.now() + relativedelta(months=1)).strftime("%Y-%m"))
        },
        # poke-interval: between <15sec> and <15min>
        "slbatch_poll_interval": 15,
        # timeouts: this for polling (+2min for the first call)
        "slbatch_api_call_timeout": 15*60,
        # [START input_files] (optional)
        "use_lc_v1": False,
        "input_files": [
            {
                # general infos
                "partner": "SAB",
                "sirc_flow_id": "SAB-01-02",
                # infos SL-Batch
                "filename_parameter_name": "inputFileName",
                # infos Landing-Component
                "partner_id": "G2S",
                "exchange_protocol": "SFTP",
                "s3_landing_path": "sab-01/sab-01-02/",
                # timedelta is not JSON serializable
                "landing_component_call_timeout": 5*60,
                "filename_pattern": "SABSLB001_*.csv",
                "expected_file_size": "4M",
                ## LC workflow customising
                "exclusive": True
            },
            {
                # general infos
                "partner": "SAB",
                "sirc_flow_id": "SAB-01-02",
                # infos SL-Batch
                "filename_parameter_name": "inputFileNameFromSAB2",
                # infos Landing-Component
                "partner_id": "g2s",
                "exchange_protocol": "SFTP",
                "s3_landing_path": "sab-01/sab-01-02/",
                "landing_component_call_timeout": 5*60,
                "filename_pattern": "SABSLB002.csv",
                "expected_file_size": "4M",
                ## LC workflow customising
                "exclusive": True
            },
            {
                # general infos
                "partner": "Franfinance",
                "sirc_flow_id": "FRA-01-01",
                # infos SL-Batch
                # "filename_parameter_name": "inputFileNameFromFranFi",
                # infos Landing-Component
                "partner_id": "franfi",
                "exchange_protocol": "CFT",
                "s3_landing_path": "btc-18/btc-18-01/",
                #'sftp_partner_path': 'path/sftp/du/partenaire/',
                "landing_component_call_timeout": 5*60,
                "filename_pattern": "FRASLB001*.csv",
                "expected_file_size": "4M",
                ## LC workflow customising
                "exclusive": False  # NOTE: utilisé par SAB,ALM,LUCY,ORA
            }
        ],
        # [START output_files] (optional)
        "output_files": [
            {
                # general infos
                "partner": "iSoft",
                "sirc_flow_id": "BTC-18-01",
                # infos SL-Batch
                "filename_parameter_name": "outputFileName",
                # infos Landing-Component
                "partner_id": "isoft",
                "exchange_protocol": "SFTP",
                "sftp_partner_path": "path/sftp/du/partenaire/",
                "landing_component_call_timeout": 5*60,
                "filename_pattern": "Isoft_" + str(datetime.now().strftime("%Y%m%d-%H%M%S-%f")) + ".txt",
                "expected_file_size": "1G"
            },
            {
                # general infos
                "partner": "iSoft",
                "sirc_flow_id": "BTC-18-01",
                # infos SL-Batch
                #"filename_parameter_name": "outputFileName",
                # infos Landing-Component
                "partner_id": "g2s",
                "exchange_protocol": "SFTP",
                "s3_landing_path": "btc-18/btc-18-01/",
                "landing_component_call_timeout": 5*60,
                "filename_pattern": "Isoft_file2_*.txt",
                "expected_file_size": "20M"
            }
        ]
    }
}

# [DAGs Creation]
for env in get_current_dag_environments(ENVIRONMENTS):
    dag = generate_dag(default_args, SCHEDULE, TAGS, DESCRIPTION, env)
    globals()[dag.dag_id] = dag
